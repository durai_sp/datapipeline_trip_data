"""
S3 connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: Mar 28, 2017

S3Utils class has the utils to copy, move, delete files and return the body of a desired s3 object.
This could be used as a connector to s3 and do the required tasks on the buckets and objects.
"""

from cStringIO import StringIO

import boto3

import logging_helper as Log

"""
:param source_bucket: Provide the source bucket name.
:param source_key: Provide the relative path of the source key path (xyz/123.gz).
:param dest_bucket: Provide the destination bucket name.
:param dest_key: Provide the destination key name (xyz/123.gz).
:param key_path: Example ('/Users/XYZ/abc.txt').
:param buffer_data: Concatenated string (buffer object).
:return:
"""


class S3Utils(object):
    def __init__(self, level):
        self.logs = Log.log(level)
        self.logs.info('S3 Utils initialized')
        self.client = boto3.client('s3')

    def copy_object_to_destination(self, source_bucket, source_key, dest_bucket, dest_key):
        self.logs.debug('Copied object - {:} from {:} to destination - {:} in this {:}'.format(source_key,source_bucket,dest_key,dest_bucket))
        self.client.copy_object(Bucket=dest_bucket, Key=dest_key, CopySource=source_bucket + '/' + source_key)

    def move_object_to_destination(self, source_bucket, source_key, dest_bucket, dest_key):
        self.logs.debug('Moved object - {:} from {:} to destination - {:} in this {:} and deleted in the destination'.format(source_key,source_bucket,dest_key,dest_bucket))
        self.client.copy_object(Bucket=dest_bucket, Key=dest_key, CopySource=source_bucket + '/' + source_key)
        self.client.delete_object(Bucket=source_bucket, Key=source_key)

    def delete_object(self, source_bucket, source_key):
        self.logs.debug('Deleted object - {:} from {:}'.format(source_key,source_bucket))
        self.client.delete_object(Bucket=source_bucket, Key=source_key)

    def object_file_size(self, source_bucket, source_key):
        self.logs.debug('Fetched object size of {:} from {:} '.format(source_key,source_bucket))
        response = self.client.head_object(Bucket=source_bucket, Key=source_key)
        return response['ContentLength']

    def upload_object(self, filename, bucket, key_path):
        self.logs.debug('Uploaded this file {:} to this object {:} in bucket'.format(filename,key_path,bucket))
        self.client.upload_file(Filename=filename, Bucket=bucket, Key=key_path)

    def download_object(self, filename, bucket, key_path):
        self.logs.debug('Downloaded this object {:} to this filepath {:} from bucket {:}'.format(key_path,filename,bucket))
        self.client.download_file(Bucket=bucket, Key=key_path, Filename=filename)

    def write_to_object(self, buffer_data, bucket, key_path):
        self.logs.debug('Wrote the buffer data to this key {:} in bucket {:}'.format(key_path,bucket))
        data_to_stream = StringIO(buffer_data)
        self.client.put_object(Bucket=bucket, Key=key_path, Body=data_to_stream.read())

    def get_object_content(self, bucket, key_path):
        self.logs.debug('Fetched the content from key {:} in bucket {:}'.format(key_path,bucket))
        key_data = self.client.get_object(Bucket=bucket, Key=key_path)
        file_contents = key_data['Body'].read()
        return file_contents

    def get_list_object(self, bucket, prefix, marker=None):
        self.logs.debug('List the object from key {:} in bucket {:}'.format(prefix,bucket))
	if marker:
	    return self.client.list_objects(Bucket=bucket, Prefix = prefix, Marker=marker)
	else:
	    return self.client.list_objects(Bucket=bucket, Prefix = prefix)
	
