from platformutils import Log
from platformutils import S3Utils


class ParseS3File():
    def __init__(self, region, level):
        self.logs = Log.log(level)
        self.logs.info('ParseS3File initialized')
        self.s3_conn = S3Utils(region, level)

    def get_dynamodb_file_data(self, bucket, key):
        data = self.s3_conn.get_object_content(bucket, key)
        return data

    def upload_parsed_dynamodb_data(self, buffer_data, bucket, key):
        data = self.s3_conn.write_to_object(buffer_data, bucket, key)
        return data

    def upload_parsed_file_to_s3(self, file_name, bucket, key):
        data = self.s3_conn.upload_object(file_name, bucket, key)
        return data
