"""
Log module.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: Mar 28, 2017

Log module for the modules and packages to implement.
"""

import logging


def log(level):
    logger = logging.getLogger(__name__)
    if not getattr(logger, 'handler_set', None):
        handler = logging.StreamHandler()
        formatter = logging.Formatter(
            '%(levelname)s %(asctime)s %(filename)s %(module)s %(funcName)s %(lineno)d %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logger.setLevel(level)
        logger.handler_set = True
    return logger
