"""
SQS connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: April 5, 2017

SQSUtils class has the necessary functions to send, read and delete messages.
"""

import boto3
import logging_helper as Log


class SQSUtils():
    def __init__(self, queue_name, level):
        self.logs = Log.log(level)
        self.logs.debug('SQS Utils initialized')
        self.queue_name = queue_name
        self.sqs_client = boto3.client('sqs')

    def _get_queue_url(self):
        self.logs.debug('Getting the queue url')
        url = self.sqs_client.get_queue_url(QueueName=self.queue_name)
        return url['QueueUrl']

    def send_message(self, body, attribute_name, json_object):
        self.logs.debug('Sending message for attribute - {:} to queue {:}'.format(attribute_name, self.queue_name))
        url = self._get_queue_url()
        # sample for attribute_name: json_object
        # attribute_name = 'Author'
        # json_object = {
        #     'StringValue': 'Daniel',
        #     'DataType': 'String'
        # }
        response = self.sqs_client.send_message(
            QueueUrl=url,
            MessageBody=body,
            DelaySeconds=5,
            MessageAttributes={attribute_name: json_object}
        )
        return response

    def read_message(self, attribute_name):
        self.logs.debug('Fetching message for attribute - {:} from queue {:}'.format(attribute_name, self.queue_name))
        url = self._get_queue_url()
        response = self.sqs_client.receive_message(
            QueueUrl=url,
            AttributeNames=['All'],
            MessageAttributeNames=[
                attribute_name,
            ],
            VisibilityTimeout=60
        )
        return response

    def delete_message(self, handle):
        self.logs.debug('Deleting the handle {:}'.format(handle))
        response = self.sqs_client.delete_message(
            QueueUrl=self._get_queue_url(),
            ReceiptHandle=handle
        )
        return response
