"""
SQLConnector module to handle connections and queries.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: Mar 28, 2017

This handles the connections and query construction once initialized.
has the necessary documentation to get started.
"""

import logging_helper as Log
from platformutils import pymysql


class SQLConnector:
    conn = None

    def __init__(self, databasehost, user, password, database, commit_value, connect_timeout, level):
        self.logs = Log.log(level)
        self.logs.info('SQLConnector initialized')
        self.databasehost = databasehost
        self.user = user
        self.password = password
        self.database = database
        self.connect_timeout = connect_timeout
        self.commit_value = commit_value
        self._connect(commit_value)

    def _connect(self, commit_value):
        try:
            self.conn = pymysql.connect(self.databasehost, user=self.user,
                                        passwd=self.password, db=self.database,
                                        autocommit=commit_value, connect_timeout=self.connect_timeout)

        except Exception as e:
            self.logs.error(e)
            raise e

    def query(self, sql, params=()):
        sql_query = ''
        for item in sql:
            sql_query += item + ' '
        try:
            self.logs.info(sql_query)
            cursor = self.conn.cursor()
            cursor.execute(sql_query, params)
            rows = cursor.fetchall()
            cursor.close()
        except (AttributeError, pymysql.OperationalError) as e:
            self.logs.error('No connection is open so new connection would be opened')
            self._connect(self.commit_value)
            cursor = self.conn.cursor()
            cursor.execute(sql_query, params)
            rows = cursor.fetchall()
            cursor.close()
        return rows

    def commit(self):
        try:
            if self.conn:
                self.conn.commit()
                self.logs.info('Commited transaction')
            else:
                self.logs.info('No Database Connection to commit changes')
        except Exception as e:
            self.logs.Error(e)
            raise e

    def close_db_connection(self):
        try:
            if self.conn:
                self.conn.close()
                self.logs.info('Closed Database Connection')
            else:
                self.logs.info('No Database Connection to Close.')
        except (AttributeError, pymysql.OperationalError) as e:
            self.logs.Error(e)
            raise e

    def start_transaction(self):
        return self.conn.begin()

    def rollback(self):
        self.conn.rollback()
        self.conn.close()

    def read_all_fields(self, table, **kwargs):
        """ Generates SQL for a SELECT statement matching the kwargs passed.
            read_all_fields("database.tablename")
            read_all_fields("database.tablename", **{"column1": "XYZ"})
            read_all_fields("database.tablename", **{"column1": "XYZ", "column2": "ABC"})
        """
        sql = list()
        sql.append("SELECT * FROM %s " % table)
        if kwargs:
            sql.append("WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return self.query(sql)

    def read_specific_fields(self, fields, table, **kwargs):
        """ Generates SQL for a SELECT statement matching the kwargs passed.
            read_specific_fields("column1, column2", "database.tablename")
            read_specific_fields("column1, column2", "database.tablename",  **{"column1": "XYZ"})
            read_specific_fields("column1, column2", "database.tablename",  **{"column1": "XYZ", "column2": "ABC"})
        """
        sql = list()
        sql.append("SELECT %s FROM %s" % (fields, table))
        if kwargs:
            sql.append(" WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return self.query(sql)

    def read_all_fields_where_less_greater_than(self, table, wherefields):
        """ Generates SQL for a SELECT statement matching the kwargs passed.
            read_specific_fields_specialcase( "database.tablename1 a, database.tablename2 b", "a.cloumn1 = 'XYZ' AND b.column < "{}"'.format(abc))
            "SELECT * FROM database.tablename1 a, database.tablename2 b WHERE a.cloumn1 = 'XYZ' AND b.column < 'abc'"
        """
        sql = list()
        sql.append("SELECT * FROM {:}".format(table))
        sql.append(" WHERE {:}".format(wherefields))
        sql.append(";")
        return self.query(sql)

    def read_specific_fields_where_less_greater_than(self, fields, table, wherefields):
        """ Generates SQL for a SELECT statement matching the kwargs passed.
            read_specific_fields_specialcase( 'a.cloumn1, b.cloumn1", "database.tablename1 a, database.tablename2 b", "a.cloumn1 = 'XYZ' AND b.column < "{}"'.format(abc))
            "SELECT a.cloumn1, b.cloumn1 FROM database.tablename1 a, database.tablename2 b WHERE a.cloumn1 = 'XYZ' AND b.column < 'abc'"
        """
        sql = list()
        sql.append("SELECT {:} FROM {:}".format(fields, table))
        sql.append(" WHERE {:}".format(wherefields))
        sql.append(";")
        return self.query(sql)

    def insert_or_update(self, table, **kwargs):
        """ update/insert rows into objects table (update if the row already exists)
            given the key-value pairs in kwargs
            insert_or_update("database.tablename", **{"column1": "value1", "column2": "value2"})
            "INSERT INTO database.tablename (column1, column2) VALUES ('value1', 'value2') ON DUPLICATE KEY UPDATE column1 = 'value1', column2 = 'value2';"
        """
        keys = ["%s" % k for k in kwargs]
        values = ["'%s'" % v for v in kwargs.values()]
        sql = list()
        sql.append("INSERT INTO %s (" % table)
        sql.append(", ".join(keys))
        sql.append(") VALUES (")
        sql.append(", ".join(values))
        sql.append(") ON DUPLICATE KEY UPDATE ")
        sql.append(", ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return self.query(sql)

    def delete(self, table, **kwargs):
        """ deletes rows from table where **kwargs match """
        sql = list()
        sql.append("DELETE FROM %s " % table)
        sql.append("WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
        sql.append(";")
        return self.query(sql)