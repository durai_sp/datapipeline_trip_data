import json

from platformutils import Log
from s3_dynamodb_parser_dal import ParseS3File
import os

class ParserWKR():

    def __init__(self, level):
        self.logs = Log.log(level)
        self.logs.info('ParserWKR initialized')
        self.parse_file = ParseS3File(level)
        self.dict = ['tripId', 'deviceId', 'distance', 'duration', 'endLat', 'endLong', 'hardAccels', 'os','startLat','startLong','status', 'suddenBrakes', 'transportMode',
            'userId', 'endUtcDtime', 'insertTime', 'startUtcDtime', 'appVersion', 'appName', 'accelerationScore', 'brakingScore', 'speedingScore', 'endBattery',
            'startBattery', 'tripScore', 'minutesSpeeding', 'phoneScore', 'insertLocalTime', 'startLocalTime', 'endLocalTime', 'tripStartReason', 'minutesPhone',
            'modifiedBy', 'insertedBy', 'eventCount', 'modifyLocalTime', 'groupCode', 'extApp', 'reasonCode', 'modifiedLocalTime', 'modifiedTime', 'ModifyTimeN',
            'osVersion', 'crashEvents', 'callEvents', 'uploadMode', 'sdkVersion', 'deviceModel', 'CS','processedTimeUtc']
        self.iter_writer = ''
        for k in self.dict:
            self.iter_writer += str(k) + ','
        self.iter_writer = self.iter_writer[:-1] + '\n'

    def convert_dynamodb_backup_tocsv(self, bucket, key, out_bucket, out_key, local_file_to_write):
        data = self.parse_file.get_dynamodb_file_data(bucket, key)
        with open(local_file_to_write,"w") as local_file:
            local_file.write(data)
        with open(local_file_to_write, "r") as local_file_read:
            for line in local_file_read:
                self.parse_line(line, local_file_to_write)
            self.write_to_object(out_bucket, out_key, local_file_to_write)

    def parse_line(self, line, local_file_to_write):
        if self.is_json(line):
            line_value = json.loads(line)
            it_writer = ''
            for k in self.dict:
                if k in line_value and 'deviceModel' in k:
                    if 'n' in line_value[k]:
                        it_writer += str(line_value[k]['n']).replace(',', '#') + ','
                    elif 's' in line_value[k]:
                        it_writer += str(line_value[k]['s']).replace(',', '#') + ','
                    else:
                        it_writer += '' + ','
                elif k in line_value and 'deviceModel' not in k:
                    if 'n' in line_value[k]:
                        it_writer += line_value[k]['n'] + ','
                    elif 's' in line_value[k]:
                        it_writer += line_value[k]['s'] + ','
                    else:
                        it_writer += '' + ','
                else:
                    it_writer += '' + ','
            # print it_writer[:-1]
            # self.iter_writer += it_writer[:-1] + '\n'
            with open(local_file_to_write + ".csv", "a") as myfile:
                myfile.write(it_writer[:-1] + '\n')

    def is_json(self, line):
        try:
            json.loads(line)
            return True
        except ValueError, e:
            return False

    def write_to_object(self, out_bucket, out_key, local_file_to_write):
        self.parse_file.upload_parsed_file_to_s3(local_file_to_write+".csv", out_bucket, out_key)
        self.iter_writer = ''
        os.remove(local_file_to_write+".csv")
        os.remove(local_file_to_write)
        #print("File Removed!", local_file_to_write)
