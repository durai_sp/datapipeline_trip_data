"""
SNS connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: Mar 28, 2017

SNSUtils class has the necessary functions to publish and read a message to sns topic.
"""

import boto3
import logging_helper as Log


class SNSUtils():
    def __init__(self, level):
        self.logs = Log.log(level)
        self.logs.info('SNS Utils initialized')
        self.sns_client = boto3.client('sns')

    def publish_message(self, arn, message):
        response = self.sns_client.publish(
            TopicArn=arn,
            Message=message
        )
        return response

    @staticmethod
    def read_message(lambda_event):
        message = ''
        topic_arn = ''
        for records in lambda_event['Records']:
            message = records['Sns']['Message']
            topic_arn = records['Sns']['TopicArn']
        return message, topic_arn
