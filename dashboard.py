from platformutils.s3_utils import S3Utils
from s3_dynamodb_parser_wkr import ParserWKR

class Dashboard:

    def __init__(self, bucket, prefix, out_bucket, out_key):
	self.bucket = bucket
	self.prefix = prefix
	self.out_bucket = out_bucket
	self.out_key = out_key
	self.s3 = S3Utils('DEBUG')
	self.local_file = "file_list.csv" 
	self.parser_wkr = ParserWKR('DEBUG')

    def dashboard(self):
	marker = None
	count = 0
	while True:
	    data = self.s3.get_list_object(self.bucket, self.prefix, marker)
	    if data['IsTruncated']:
		for content in data['Contents']:
	    	    count = count + 1
     	    	    name = content['Key'].rsplit('/', 1)
	    	    marker = content['Key']
		    self.write_object(name[1])
            else:
		for content in data['Contents']:
	    	    count = count + 1
     	    	    name = content['Key'].rsplit('/', 1)
	    	    marker = content['Key']
		    self.write_object(name[1])
		break

    def write_object(self, data):
	if data != 'manifest' and data != '_SUCCESS':
	    with open(self.local_file, "a") as myfile:
	    	myfile.write(data + "\n")
	
    def process_data(self):
	with open(self.local_file) as path_file:
            for line_with_nextline_char in path_file:
            	line = line_with_nextline_char.strip('\n')
		self.parser_wkr.convert_dynamodb_backup_tocsv(self.bucket, self.prefix+line, self.out_bucket, self.out_key+line+'.csv', line)


#bucket = 'policypalacnprod'
#prefix = 'agero/prod/datapipeline/dynamodb-backup/trips-metadata-complete-backup/2017-07-25-14-56-36/'
#out_bucket = 'policypalacnprod'
#out_key = 'agero/prod/datapipeline/dynamodb-backup/trips-metadata-complete-backup/2017-07-25-14-56-36-output/'
#d = Dashboard(bucket, prefix, out_bucket, out_key)
#d.dashboard()
#d.process_data()
