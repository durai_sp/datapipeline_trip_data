from dashboard import Dashboard

bucket = 'policypalacnprod'
prefix = 'agero/prod/datapipeline/dynamodb-backup/trips-metadata-complete-backup/2017-07-25-14-56-36/'
out_bucket = 'policypalacnprod'
out_key = 'agero/prod/datapipeline/dynamodb-backup/trips-metadata-complete-backup/2017-07-25-14-56-36-output/'

def handler():
    d = Dashboard(bucket, prefix, out_bucket, out_key)
    d.dashboard()
    d.process_data()

handler()
